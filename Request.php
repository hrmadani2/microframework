<?php

class Request
{
    //Url form is: protocol://domain.tld/somename/params
    public $url;
    //Set Default Controller , Method and Parameters
    public $controller = "WelcomeController";
    public $method = "index";
    public $params = [];

    //Not Found Page
    public $notFound = false;

    public function __construct()
    {
        $this->url = $_GET['url'];
    }
}