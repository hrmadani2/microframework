<?php
namespace App\Controllers;


use Core\Controller;

class WelcomeController extends Controller
{
    /*
     * The default page
     * */
    public function index()
    {
        $this->view("Welcome");
    }

    /*
     * Print Every Inputs
     * */
    public function printer(array $input){
        //Can Do every Logical Operation Before Show Data To End-User
        $this->view("Printer",$input);
    }
}