<?php

class Router
{
    public static function render($url, $request)
    {
        $routes = Router::RoutesProvider('web');
        if ($url != "") {
            //Explode URL
            $url = explode("/", $url);
            //Detect the Rout's name and Params
            $name = strtolower($url[0]);
            //Remove the name from url
            //after it only need params
            unset($url[0]);
            //Check the received name with routes list
            if(array_key_exists($name , $routes)){
                $route = $routes[$name];
                //Check Http Request Method
                if($_SERVER['REQUEST_METHOD'] != strtoupper($route['Http'])){
                    $request->notFound = true;
                    return;
                }
                //Set Controller, Method and Params
                $request->controller = $route['Controller'];
                $request->method = $route['Method'];
                $request->params = $url;
                return;
            }
            $request->notFound = true;
        }
    }

    /*
     * Provide the Routes Address
     */
    public static function RoutesProvider($routes){
        $routesFile = BASE_DIR."Core/Routes/$routes.php";
        include $routesFile;
        return $routes;
    }
}