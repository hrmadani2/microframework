<?php
namespace Core;


class Controller
{
    private $valuale = [];
    private $header = BASE_DIR."App/Views/inc/header.php";
    private $footer = BASE_DIR."App/Views/inc/footer.php";

    /**
     * @param mixed $valuale
     */
    private function setValuale($valuale)
    {
        $this->valuale = array_merge($this->valuale , $valuale);
    }

    /*
     * Render the html Page
     * */
    public function view($page , $value = [])
    {
        $this->setValuale($value);
        $value = ['input' => $value];
        extract($value);
        ob_start();

        $viewFile = BASE_DIR."App/Views/".$page.".php";
        include $this->header;
        include $viewFile;
        include $this->footer;
    }
}