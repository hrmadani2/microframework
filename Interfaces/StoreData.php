<?php

namespace Interfaces;


interface StoreData
{
    /**
     * Store Data in a Data Store
     *
     * @param $handler : Database or File Handler
     * @param $data
     * @return mixed
     */
    public function storeData($handler , $data);
}