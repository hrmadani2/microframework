<?php

namespace Interfaces;


interface UpdateData
{
    /**
     * Update Data in a Data Store
     *
     * @param $handler : Database or File Handler
     * @param $condition : Which data that you want to update
     * @param $data
     * @return mixed
     */
    public function updateData($handler , $condition , $data);
}